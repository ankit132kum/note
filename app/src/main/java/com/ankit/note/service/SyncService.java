package com.ankit.note.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ankit.note.model.NoteModel;
import com.ankit.note.provider.DataProvider;
import com.ankit.note.utility.ProviderCallback;
import com.ankit.note.utility.Result;

import java.util.ArrayList;

/**
 * Created by ankitkumar on 29/06/16.
 */

public class SyncService extends Service{

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        DataProvider.getInstance(this).pendingNotes(new ProviderCallback() {
            @Override
            public void OnActionCompleted(final Result result) {
                if (result.isSuccess == true){

                    ArrayList<NoteModel> notes = (ArrayList<NoteModel>) result.object;
                    syncNotesToServers(notes);

                }else{
                    Log.e("SyncService", result.errorMessage);
                    stopSelf();
                }
            }
        });

        return super.onStartCommand(intent, flags, startId);
    }

    public void syncNotesToServers(ArrayList<NoteModel> notes){

        DataProvider.getInstance(this).updateNoteStatus(notes, 1);
        DataProvider.getInstance(this).syncNoteToServer(notes, new ProviderCallback() {
            @Override
            public void OnActionCompleted(Result result) {
                stopSelf();
            }
        });
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }
}
