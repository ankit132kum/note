package com.ankit.note.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ankit.note.R;

import com.ankit.note.model.NoteModel;


import java.util.ArrayList;

/**
 * Created by ankitkumar on 26/06/16.
 */
public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {

    private ArrayList<NoteModel> mDataset;

    // Provide a suitable constructor (depends on the kind of dataset)
    public NoteAdapter(ArrayList<NoteModel> myDataset) {
        this.mDataset = myDataset;
    }

    public void setmDataset(ArrayList<NoteModel> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_card, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        NoteModel noteModel = mDataset.get(position);
        holder.title.setText(noteModel.getTitle());
        holder.subject.setText(noteModel.getSubject());
        holder.description.setText(noteModel.getDescription());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView subject;
        public TextView description;

        public ViewHolder(View v) {
            super(v);
            this.title = (TextView) v.findViewById(R.id.info_text);
            this.subject = (TextView) v.findViewById(R.id.info_subject_text);
            this.description = (TextView) v.findViewById(R.id.info_description_text);
        }
    }
}
