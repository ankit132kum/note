package com.ankit.note.utility;

/**
 * Created by ankitkumar on 28/06/16.
 */

public interface ProviderCallback {

    public void OnActionCompleted(Result result);
}
