package com.ankit.note.utility;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by ankitkumar on 28/06/16.
 */

public class Utils {

    public static void runOnBackgroundThread(final Runnable R) {

        Thread th = new Thread() {

            @Override
            public void run() {
                R.run();
            }
        };
        th.start();
    }

    public static void runOnMainThread(Runnable runnable) {

        if (Looper.myLooper() == Looper.getMainLooper()) {

            runnable.run();
        } else {

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(runnable);

        }
    }

}
