package com.ankit.note.provider;

import com.ankit.note.model.NoteModel;
import com.ankit.note.utility.CustomException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ankitkumar on 28/06/16.
 */

public class ResponseParser {

    private static final String RESPONSE_EMPTY = "Resonse Empty";
    private static final String RESPONSE_PARSE_ERROR = "Response Format Error";
    private static final String DATA_PARSE_ERROR = "Expected Data Not Received";
    private static final String NO_DATA_FOUND = "Data is Empty";

    private static JSONObject parseResponse(String str) throws CustomException {

        if (str.isEmpty()){ throw new CustomException(RESPONSE_EMPTY);}

        JSONObject resObj;

        try {
            resObj = new JSONObject(str);

            Boolean isSuccess = resObj.getBoolean("success");

            if (isSuccess == false){
                String message = resObj.getString("message");
                throw new CustomException(message);
            }

        } catch (JSONException e){
            throw new CustomException(RESPONSE_PARSE_ERROR);
        }

        return resObj;
    }


    public static ArrayList<NoteModel> parseNotes(String response) throws CustomException {

        JSONObject resObj = parseResponse(response);
        ArrayList<NoteModel> notes = new ArrayList<>();

        try {

            JSONArray array = resObj.getJSONArray("data");

            if(array.length() == 0){ throw new CustomException(NO_DATA_FOUND);}

            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.getJSONObject(i);

                NoteModel n = new NoteModel();
                n.setTitle(obj.getString("title"));
                n.setSubject(obj.getString("subject"));
                n.setDescription(obj.getString("description"));

                notes.add(n);
            }

        } catch (JSONException e) {
            throw new CustomException(DATA_PARSE_ERROR);
        }

        return notes;
    }

    public static Boolean isSynced(String response) throws CustomException, JSONException {

        JSONObject resObj = parseResponse(response);

        return resObj.getBoolean("success");
    }
}
