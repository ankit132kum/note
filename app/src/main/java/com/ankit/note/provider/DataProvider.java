package com.ankit.note.provider;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ankit.note.database.DAO;
import com.ankit.note.model.NoteModel;
import com.ankit.note.network.RequestMaker;
import com.ankit.note.utility.CustomException;
import com.ankit.note.utility.ProviderCallback;
import com.ankit.note.utility.Result;
import com.ankit.note.utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by ankitkumar on 28/06/16.
 */
public class DataProvider {

    private static final String INTERNET_ERROR = "Connectivity Issue";

    private static DataProvider dataProvider;
    public Context mContext;

    public static DataProvider getInstance(Context context) {
        if (dataProvider == null) {
            dataProvider = new DataProvider(context);
        }
        return dataProvider;
    }

    private DataProvider(Context context) {
        mContext = context.getApplicationContext();
    }

    public Boolean isInternetActive(){

        ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }

        return false;
    }

    public void getLocalNotes(final ProviderCallback providerCallback){

        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {

                Result result = new Result();

                try {
                    ArrayList<NoteModel> notes = DAO.getHelper(mContext).getAllNotes();

                    if (notes.isEmpty()) {
                        result.errorMessage = "EMPTY_RESULT_SET";
                    }else {
                        result.isSuccess = true;
                        result.object = notes;
                    }
                }catch (CustomException e){
                    result.errorMessage = e.getMessage();
                }

                providerCallback.OnActionCompleted(result);
            }
        });
    }

    public void pendingNotes(final ProviderCallback providerCallback){

        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {

                Result result = new Result();

                try {
                    ArrayList<NoteModel> notes = DAO.getHelper(mContext).pendingNotes();

                    if (notes.isEmpty()) {
                        result.errorMessage = "EMPTY_RESULT_SET";
                    }else {
                        result.isSuccess = true;
                        result.object = notes;
                    }
                }catch (CustomException e){
                    result.errorMessage = e.getMessage();
                }

                providerCallback.OnActionCompleted(result);
            }
        });
    }

    public void getServerNotes(final ProviderCallback providerCallback){

        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {

                Result result = new Result();

                if (!isInternetActive()){
                    result.isSuccess = false;
                    result.errorMessage = INTERNET_ERROR;
                    providerCallback.OnActionCompleted(result);
                    return;
                }

                try{
                    ArrayList<NoteModel> notes = ResponseParser.parseNotes(RequestMaker.getAllNotes());
                    result.isSuccess = true;
                    result.object = notes;
                }catch (CustomException e){
                    result.errorMessage = e.getMessage();
                }

                providerCallback.OnActionCompleted(result);
            }
        });
    }

    public void syncNoteToServer(final ArrayList<NoteModel> notes, final ProviderCallback providerCallback){

        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {

                Result result = new Result();

                try{

                    JSONArray jsonArray = new JSONArray();

                    for (NoteModel n: notes) {
                        jsonArray.put(n.getJSONObject());
                    }

                    if (jsonArray.length() == 0 ){
                        if (providerCallback != null){
                            providerCallback.OnActionCompleted(result);
                        }
                        return;
                    }

                    if (isInternetActive()){
                        result.isSuccess = ResponseParser.isSynced(RequestMaker.syncNotes(jsonArray.toString()));
                    }

                    if ( !isInternetActive() || result.isSuccess == false){
                        DAO.getHelper(mContext).updateSyncedNotesWithStatus(notes, 0);
                    }else {
                        DAO.getHelper(mContext).updateSyncedNotesWithStatus(notes, 1);
                    }

                }catch (CustomException e){
                    result.errorMessage = e.getMessage();
                }catch (JSONException j){
                    result.errorMessage = "Expected Data Not Received";
                }

                if (providerCallback != null){
                    providerCallback.OnActionCompleted(result);
                }

            }
        });

    }

    public void addNotes(final ArrayList<NoteModel> notes, final ProviderCallback providerCallback){
        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {

                Result result = new Result();

                for (NoteModel n :notes) {
                    long row = DAO.getHelper(mContext).insertNote(n);
                    n.setId(row+"");
                }

                syncNoteToServer(notes, null);

                result.isSuccess = true;
                providerCallback.OnActionCompleted(result);
            }
        });
    }

    public void updateNoteStatus(final ArrayList<NoteModel> notes, final int status){

        Utils.runOnBackgroundThread(new Runnable() {
            @Override
            public void run() {
                DAO.getHelper(mContext).updateSyncedNotesWithStatus(notes, status);
            }
        });

    }

}
