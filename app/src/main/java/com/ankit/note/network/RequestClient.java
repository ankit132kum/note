package com.ankit.note.network;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by ankitkumar on 28/06/16.
 */

public class RequestClient {

    private static final OkHttpClient client = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static String getAbsoluteUrl(String relativeUrl) {
        return RequestMaker.BASE_URL + relativeUrl;
    }

    public static String executeGetRequest(String URL) {

        String result= "";
        try {
            Request request = new Request.Builder().url(getAbsoluteUrl(URL)).build();
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String executePostRequest(String URL, String json) {

        String result= "";
        try {
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder().url(getAbsoluteUrl(URL)).post(body).build();
            Response response = client.newCall(request).execute();
            result = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
