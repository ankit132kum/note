package com.ankit.note.network;

/**
 * Created by ankitkumar on 28/06/16.
 */

public class RequestMaker {

    public static final String BASE_URL = "http://www.aayushbhatnagar.com/";

    private static final String URL_ALL_NOTES = "getAllNotes.php";
    private static final String URL_SYNC_NOTES = "addNotes.php";

    public static String getAllNotes(){
        return RequestClient.executeGetRequest(URL_ALL_NOTES);
    }

    public static String syncNotes(String jsonString){
        return RequestClient.executePostRequest(URL_SYNC_NOTES , jsonString);
    }
}
