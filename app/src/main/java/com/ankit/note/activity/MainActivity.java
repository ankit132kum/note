package com.ankit.note.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ankit.note.R;
import com.ankit.note.adapter.NoteAdapter;
import com.ankit.note.model.NoteModel;
import com.ankit.note.provider.DataProvider;
import com.ankit.note.service.SyncService;
import com.ankit.note.utility.ProviderCallback;
import com.ankit.note.utility.Result;
import com.ankit.note.utility.Utils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int SAVE_CODE = 69;

    private RecyclerView mRecyclerViewLocal;
    private RecyclerView mRecyclerViewRemote;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManagerRemote;

    private ArrayList<NoteModel> parseObjectArrayListLocal;
    private ArrayList<NoteModel> parseObjectArrayListRemote;

    private CoordinatorLayout coordinatorLayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    private NoteAdapter mAdapterLocal;
    private NoteAdapter mAdapterRemote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        syncData();

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NoteSaveActivity.class);
                startActivityForResult(intent, SAVE_CODE);
            }
        });

        parseObjectArrayListLocal = new ArrayList<>();
        parseObjectArrayListRemote = new ArrayList<>();


        mRecyclerViewLocal = (RecyclerView) findViewById(R.id.my_recycler_view_local);
        mRecyclerViewRemote =  (RecyclerView) findViewById(R.id.my_recycler_view_server);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewLocal.setHasFixedSize(true);
        mRecyclerViewRemote.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewLocal.setLayoutManager(mLayoutManager);

        mLayoutManagerRemote = new LinearLayoutManager(this);
        mRecyclerViewRemote.setLayoutManager(mLayoutManagerRemote);

        // specify an adapter (see also next example)
        mAdapterLocal = new NoteAdapter(parseObjectArrayListLocal);
        mRecyclerViewLocal.setAdapter(mAdapterLocal);

        mAdapterRemote = new NoteAdapter(parseObjectArrayListRemote);
        mRecyclerViewRemote.setAdapter(mAdapterRemote);

        loadDataFromLocal();
        
        loadDataFromServer();

    }

    private void syncData() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Intent service = new Intent(MainActivity.this, SyncService.class);
            MainActivity.this.startService(service);
        }

    }

    private void loadDataFromLocal() {


        DataProvider.getInstance(this).getLocalNotes(new ProviderCallback() {
            @Override
            public void OnActionCompleted(final Result result) {

                Utils.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result.isSuccess == true){
                            parseObjectArrayListLocal.clear();
                            parseObjectArrayListLocal.addAll((ArrayList<NoteModel>) result.object);

                            mAdapterLocal.setmDataset(parseObjectArrayListLocal);

                        }else {
                            Log.e(TAG,result.errorMessage);
                        }
                    }
                });
            }
        });

    }

    private void loadDataFromServer() {

        final Snackbar snackbar = Snackbar.make(coordinatorLayout, "Loading Data From Server...", Snackbar.LENGTH_INDEFINITE);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();

        DataProvider.getInstance(this).getServerNotes(new ProviderCallback() {
            @Override
            public void OnActionCompleted(final Result result) {

                Utils.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {

                        if (result.isSuccess == true) {
                            parseObjectArrayListRemote.clear();
                            parseObjectArrayListRemote.addAll((ArrayList<NoteModel>) result.object);

                            mAdapterRemote.setmDataset(parseObjectArrayListRemote);

                        }else{
                            Log.e(TAG,result.errorMessage);
                        }

                        snackbar.dismiss();

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                });

            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SAVE_CODE) {
            if (resultCode == RESULT_OK) {
                loadDataFromLocal();
                loadDataFromServer();
            }
        }
    }

    @Override
    public void onRefresh() {
        loadDataFromServer();
    }
}
