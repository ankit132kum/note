package com.ankit.note.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ankit.note.R;
import com.ankit.note.model.NoteModel;
import com.ankit.note.provider.DataProvider;
import com.ankit.note.utility.ProviderCallback;
import com.ankit.note.utility.Result;

import java.util.ArrayList;

public class NoteSaveActivity extends AppCompatActivity {

    private EditText editTitle;
    private EditText editSubject;
    private EditText editDescription;

    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        editTitle = (EditText) findViewById(R.id.edit_title);
        editSubject = (EditText) findViewById(R.id.edit_subject);
        editDescription = (EditText) findViewById(R.id.edit_description);

        saveButton = (Button) findViewById(R.id.button_save);

        saveButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                validateSaveObject();
            }

            private void validateSaveObject() {

                String title = editTitle.getText().toString();
                String subject = editSubject.getText().toString();
                String description = editDescription.getText().toString();

                if (title.isEmpty() || subject.isEmpty() || description.isEmpty()) {
                    Toast.makeText(NoteSaveActivity.this,"All feilds are Compulsory",Toast.LENGTH_LONG).show();
                    return;
                }

                NoteModel noteObject = new NoteModel();
                noteObject.setTitle(title);
                noteObject.setSubject(subject);
                noteObject.setDescription(description);
                noteObject.setSynced(false);

                ArrayList<NoteModel> notes = new ArrayList<>(1);
                notes.add(noteObject);

                DataProvider.getInstance(NoteSaveActivity.this).addNotes(notes, new ProviderCallback() {
                    @Override
                    public void OnActionCompleted(Result result) {
                        if (result.isSuccess) {
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
            }
        });

    }

}
