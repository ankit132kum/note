package com.ankit.note.database;

import android.provider.BaseColumns;

/**
 * Created by ankitkumar on 23/06/16.
 */

public class NoteContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public NoteContract() {}

    /* Inner class that defines the table contents */
    public static abstract class Note implements BaseColumns {
        public static final String TABLE_NAME = "Note";
        public static final String COLUMN_NAME_TITLE = "Title";
        public static final String COLUMN_NAME_SUBJECT = "Subject";
        public static final String COLUMN_NAME_DESCRIPTION = "Description";
        public static final String COLUMN_IS_SYNCED = "Sync";
    }
}

