package com.ankit.note.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ankitkumar on 23/06/16.
 */

public class NoteDatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Wardrobe.db";


    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_SHIRT_ENTRIES =
            "CREATE TABLE " + NoteContract.Note.TABLE_NAME + " (" +
                    NoteContract.Note._ID + " INTEGER PRIMARY KEY," +
                    NoteContract.Note.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    NoteContract.Note.COLUMN_NAME_SUBJECT + TEXT_TYPE + COMMA_SEP +
                    NoteContract.Note.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    NoteContract.Note.COLUMN_IS_SYNCED + INT_TYPE + " DEFAULT 0" + " )";

    private static final String SQL_DELETE_SHIRT_ENTRIES =
            "DROP TABLE IF EXISTS " + NoteContract.Note.TABLE_NAME;


    public NoteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SHIRT_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_SHIRT_ENTRIES);
        onCreate(db);
    }
}
