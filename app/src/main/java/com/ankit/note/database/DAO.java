package com.ankit.note.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ankit.note.model.NoteModel;
import com.ankit.note.utility.CustomException;

import java.util.ArrayList;

/**
 * Created by ankitkumar on 23/06/16.
 */

public class DAO {

    private SQLiteDatabase database;
    private NoteDatabaseHelper dbHelper;

    private static DAO dataSource;

    public static synchronized DAO getHelper(Context context){
        if (dataSource == null) {
            dataSource = new DAO(context.getApplicationContext());
        }
        return dataSource;
    }

    private DAO(Context context){
        dbHelper = new NoteDatabaseHelper(context);
        getWritableDatabase();
    }

    private void getWritableDatabase() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public ArrayList<NoteModel> getAllNotes() throws CustomException {

        ArrayList<NoteModel> noteModels = new ArrayList<>();

        Cursor cursor = null;

        try {
            cursor = database.query( NoteContract.Note.TABLE_NAME, null, null, null,null, null, NoteContract.Note._ID +" DESC");

            if(cursor == null) {throw new CustomException("Empty Result");}

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    NoteModel noteModel = new NoteModel();
                    noteModel.setId(cursor.getString(0));
                    noteModel.setTitle(cursor.getString(1));
                    noteModel.setSubject(cursor.getString(2));
                    noteModel.setDescription(cursor.getString(3));
                    noteModel.setSynced(cursor.getInt(4) == 0 ? false : true);

                    noteModels.add(noteModel);
                } while (cursor.moveToNext());
            }

        } catch (SQLException s){
            throw new CustomException("SQL Exception");
        }
        finally {
            if (cursor != null) {cursor.close();}
        }

        return noteModels;

    }

    public ArrayList<NoteModel> pendingNotes() throws CustomException {

        ArrayList<NoteModel> noteModels = new ArrayList<>();

        Cursor cursor = null;

        String query = "Select * from " + NoteContract.Note.TABLE_NAME + " where Sync=0";

        try {
            cursor = database.rawQuery(query,null); //database.query( NoteContract.Note.TABLE_NAME, null, null, null,null, null, null);

            if(cursor == null) {throw new CustomException("Empty Result");}

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    NoteModel noteModel = new NoteModel();
                    noteModel.setId(cursor.getString(0));
                    noteModel.setTitle(cursor.getString(1));
                    noteModel.setSubject(cursor.getString(2));
                    noteModel.setDescription(cursor.getString(3));
                    noteModel.setSynced(cursor.getInt(4) == 0 ? false : true);

                    Log.d("Pril" , noteModel.toString());

                    noteModels.add(noteModel);
                } while (cursor.moveToNext());
            }

        } catch (SQLException s){
            throw new CustomException("SQL Exception");
        }
        finally {
            if (cursor != null) {cursor.close();}
        }

        return noteModels;

    }

    public long insertNote(NoteModel noteModel) {

        ContentValues values = new ContentValues();
        values.put(NoteContract.Note.COLUMN_NAME_TITLE, noteModel.getTitle());
        values.put(NoteContract.Note.COLUMN_NAME_SUBJECT, noteModel.getSubject());
        values.put(NoteContract.Note.COLUMN_NAME_DESCRIPTION, noteModel.getDescription());
        values.put(NoteContract.Note.COLUMN_IS_SYNCED, noteModel.getSynced());

        // Inserting Row
        return database.insert(NoteContract.Note.TABLE_NAME, null, values);
    }

    public void updateSyncedNotesWithStatus(ArrayList<NoteModel> notes, int status){

        for (NoteModel n: notes) {

            ContentValues values = new ContentValues();
            values.put(NoteContract.Note.COLUMN_NAME_TITLE, n.getTitle());
            values.put(NoteContract.Note.COLUMN_NAME_SUBJECT, n.getSubject());
            values.put(NoteContract.Note.COLUMN_NAME_DESCRIPTION, n.getDescription());
            values.put(NoteContract.Note.COLUMN_IS_SYNCED, status);

            database.update(NoteContract.Note.TABLE_NAME, values ,NoteContract.Note._ID + " = ?",new String[] { String.valueOf(n.getId())});

        }

    }
}
